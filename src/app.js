App = {
    loading: false,
    contracts: {},

    load: async () => {
        // Load app...
        await App.loadWeb3()
        await App.loadAccount()
        await App.loadContract()
        await App.render()
    },

    // https://medium.com/metamask/https-medium-com-metamask-breaking-change-injecting-web3-7722797916a8
    loadWeb3: async () => {
        if (typeof web3 !== 'undefined') {
            App.web3Provider = web3.currentProvider
            web3 = new Web3(web3.currentProvider)
        } else {
            window.alert("Please connect to Metamask.")
            // If no injected web3 instance is detected, fallback to Ganache.
           App.web3Provider = new web3.providers.HttpProvider('http://127.0.0.1:7545');
           web3 = new Web3(App.web3Provider);
        }
        
    },

    loadAccount: async () => {
        // set the current blockchain account
        App.account = web3.eth.accounts[0]
    },

    loadContract: async () => {
        // Create a JavaScript version of the smart contract
        const todoList = await $.getJSON('TodoList.json')
        //var todo = require("/Users/kemlohalex/Desktop/projects/eth-todo-list/contracts/TodoList.sol");
        //console.log(todo)
        App.contracts.TodoList = TruffleContract(todoList)
        App.contracts.TodoList.setProvider(App.web3Provider)

        // Hydrate the smart contract with values from the blockchain
        App.todoList = await App.contracts.TodoList.deployed()
    },

    render: async () => {
        // Prevent double render
        if (App.loading) {
            return
        }

        // Update app loading state
        App.setLoading(true)

        // Render Account
        $('#account').html(App.account)

        // Render Tasks
        await App.renderTasks()

        // Update app loading state
        App.setLoading(false)
    },

    createTask: async () => {
        App.setLoading(true)
        const content = $('#newTask').val()
        await App.todoList.createTask(content)
        window.location.reload()
    },

    toggleCompleted: async (task) => {
        try {
        App.setLoading(true)
        const taskId = task.target.id
        await App.todoList.toggleCompleted(taskId)
        window.location.reload()
        } catch(e) {
            window.alert(e.message)
            window.location.reload()
            //task.target.checked = true
            //App.setLoading(false)
        }
        
    },

    setLoading: (boolean) => {
        App.loading = boolean
        const loader = $('#loader')
        const content = $('#content')
        if(boolean) {
            loader.show()
            content.hide()
        }
        else {
            loader.hide()
            content.show()
        }
    },

    renderTasks: async () => {
        // Load the total task count from the blockchain
        const taskCount = await App.todoList.taskCount()
        const $taskTemplate = $('.taskTemplate')

        // Render out each task  with a new task template
        for (var i = 1; i <= taskCount; i++) {
            // Fetch the task data from the blockchain
            const task = await App.todoList.tasks(i)
            const taskId = task[0].toNumber()
            const taskContent = task[1]
            const taskCompleted = task[2]

            // Create the html for the task
            const $newTaskTemplate = $taskTemplate.clone()
            $newTaskTemplate.find('.content').html(taskContent)
            $newTaskTemplate.find('input')
                            .prop('id', taskId)
                            .prop('checked', taskCompleted)
                            .on('click', App.toggleCompleted)
                        
            // Put the task in the correct list
            if (taskCompleted) {
                $('#completedTaskList').append($newTaskTemplate)
            }
            else {
                $('#taskList').append($newTaskTemplate)
            }

            // show the task
        $newTaskTemplate.show()
        
        }
    },

}

$(() => {
    $(window).load(() => {
        App.load();
    })
})