# ETHEREUM TODOLIST #

This is a simple todo dapp running on the ethereum blockchain.

### How do I get set up? ###

* Make sure you have ganache running
* Please use a browser like Google chrome and a wallet like Metamask for web3 injection
* "truffle test" will run all the tests
* Start the server by running "npm run dev"
* Once the dapp started you can add a new task to the todo list or delete a existing task from the todo list
* Make sure to confirm the transaction when the Metamask require it
